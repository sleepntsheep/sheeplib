#include "bitset.h"

static inline size_t bitslot(size_t b) { return b / BITINT_BIT; }
static inline size_t bitmask(size_t b) { return 1u << (b % BITINT_BIT); }

void bitset(bitint *a, size_t b)
{
    a[bitslot(b)] |= bitmask(b);
}

void bitunset(bitint *a, size_t b)
{
    a[bitslot(b)] &= ~bitmask(b);
}

int bitget(bitint *a, size_t b)
{
    return a[bitslot(b)] & bitmask(b);
}

