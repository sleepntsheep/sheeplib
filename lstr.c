
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lstr.h"

static size_t round2n(size_t x) {
    int i = 0;
    for (; (1ull << i) < x; i++);
    return (1 << i);
}

static lstr lstr_ensure_alloc(lstr string, size_t enough_for) {
    size_t newalloc = round2n(enough_for + lstr_length(string));
    return lstr_realloc(string, newalloc);
}

struct string {
    size_t alloc;
    size_t len;
    char buf[];
};

static struct string *lstr_getinfo(lstr string) {
    return (struct string*)((char*)string - sizeof(struct string));
}

size_t lstr_alloc(lstr string) {
    return lstr_getinfo(string)->alloc;
}

size_t lstr_length(lstr string) {
    return lstr_getinfo(string)->len;
}

lstr lstr_make(const char *cstr) {
    if (cstr == NULL) return NULL;
    size_t len = strlen(cstr);
    struct string *string = malloc(sizeof *string + len + 1);
    if (!string) return NULL;
    string->alloc = len + 1;
    string->len = len;
    memcpy(string->buf, cstr, len);
    string->buf[len] = 0;
    return string->buf;
}

lstr lstr_clone(const char *cstr)
{
    return lstr_make(cstr);
}

lstr lstr_makelen(const char *cstr, size_t len)
{
    lstr s = lstr_make("");
    s = lstr_catlen(s, cstr, len);
    return s;
}

lstr lstr_realloc(lstr string, size_t newalloc) {
    if (string == NULL) return NULL;
    if (newalloc < lstr_alloc(string)) {
        return string;
    }
    lstr_getinfo(string)->alloc = newalloc;
    struct string *newstring = realloc(lstr_getinfo(string), sizeof(struct string) + newalloc);
    return newstring->buf;
}

lstr lstr_cat(lstr string, const char *cstr) {
    return lstr_catlen(string, cstr, strlen(cstr));
}

lstr lstr_catlen(lstr string, const char *cstr, size_t len) {
    if (string == NULL) return NULL;
    string = lstr_ensure_alloc(string, len + 1);
    memcpy(string + lstr_length(string), cstr, len);
    string[lstr_length(string) + len] = 0;
    lstr_getinfo(string)->len += len;
    return string;
}

lstr lstr_asprintf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    size_t len = vsnprintf(NULL, 0, fmt, args);
    va_end(args);
    struct string *string = malloc(sizeof *string + len + 1);
    string->alloc = len + 1;
    string->len = len;
    va_start(args, fmt);
    vsnprintf(string->buf, len + 1, fmt, args);
    va_end(args);
    return string->buf;
}

void lstr_drop(lstr *s) {
    if (!s || !*s) return;
    free(lstr_getinfo(*s));
}


