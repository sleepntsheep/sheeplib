#pragma once
#ifndef LSTR_H
#define LSTR_H

#include <stddef.h>

typedef char *lstr;

lstr lstr_make(const char *cstr);
lstr lstr_clone(const char *cstr);
lstr lstr_makelen(const char *cstr, size_t len);
lstr lstr_format(const char *fmt, ...);
lstr lstr_cat(lstr string, const char *cstr);
lstr lstr_catlen(lstr string, const char *cstr, size_t len);
lstr lstr_realloc(lstr string, size_t newalloc);
size_t lstr_length(lstr string);
void lstr_drop(lstr *string);
lstr lstr_asprintf(const char *fmt, ...);

#endif

