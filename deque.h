#if !defined(l_con) || !defined(l_val)
#error "Define l_con and l_val before including"
#endif

#if !defined(l_con)
#define l_con Ccat2(dynarr, l_val)
#endif

#define l_itr Ccat2(l_con, itr)

#if (defined(l_valdrop) && !defined(l_valclone)) || (defined(l_valclone) && !defined(l_valdrop))
#error "l_valdrop and l_valclone are mutually inclusive"
#endif

/* preprocessor magic */
#define Ccat3(a,b) a##_##b
#define Ccat2(a,b) Ccat3(a,b)
#define Cpref(x) Ccat2(l_con,x)

#ifndef l_valdrop
#define l_valdrop
#endif
#ifndef l_valclone
#define l_valclone(x) x
#endif

#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>

typedef struct
{
    size_t alloc;
    size_t head;
    size_t tail;
    l_val *data;
} l_con;

typedef struct
{
    l_con *a;
    l_val *ref;
} l_itr;

static inline l_con Cpref(init)(void)
{
    l_con q;
    q.alloc = 0;
    q.data = NULL;
    q.head = 0;
    q.tail = 0;
    return q;
}

static inline void Cpref(drop)(l_con *q)
{
    free(q->data);
}

static inline size_t Cpref(size)(l_con *q)
{
    if (q->alloc && (q->head + 1) % q->alloc == q->tail) return q->alloc - 1;
    if (q->head >= q->tail) return q->head - q->tail;
    return q->head + q->alloc - q->tail;
}

static inline size_t Cpref(empty)(l_con *q)
{
    return Cpref(size)(q) == 0;
}

static inline void Cpref(clear)(l_con *q)
{
    q->head = q->tail = 0;
}

static void Cpref(grow)(l_con *q, size_t n)
{
    size_t sz = Cpref(size)(q);
    if (sz + n + 1 <= q->alloc) return;
    size_t old_alloc = q->alloc;
    q->alloc = q->alloc ? q->alloc * 2 : 32;
    q->data = (l_val*)realloc(q->data, q->alloc * sizeof(l_val));
    if (q->head >= q->tail)
    {
        memmove(q->data, q->data + q->tail, (q->head - q->tail) * sizeof(l_val));
        q->tail = 0;
        q->head = sz;
    }
    else
    {
        size_t pt = old_alloc - q->tail + 1;
        memcpy(q->data + old_alloc + q->tail, q->data + q->tail, pt * sizeof(l_val));
        q->tail += old_alloc;
    }
}

static inline void Cpref(push_back)(l_con *q, l_val x)
{
    Cpref(grow)(q, 1);
    q->data[q->head] = l_valclone(x);
    if (++q->head == q->alloc) q->head = 0;
}

static inline void Cpref(pop_back)(l_con *q)
{
    assert(Cpref(size)(q));
    if (q->head > 0) 
        --q->head;
    else q->head = q->alloc - 1;
    l_valdrop(q->data[q->head]);
}

static inline void Cpref(push_front)(l_con *q, l_val x)
{
    Cpref(grow)(q, 1);
    if (q->tail > 0) --q->tail; else q->tail = q->alloc - 1;
    q->data[q->tail] = l_valclone(x);
}

static inline void Cpref(pop_front)(l_con *q)
{
    assert(Cpref(size)(q));
    l_valdrop(q->data[q->tail]);
    if (++q->tail == q->alloc) q->tail = 0;
}

static inline l_val Cpref(back)(l_con *q)
{
    assert(Cpref(size)(q));
    return q->head > 0 ? q->data[q->head - 1] : q->data[q->alloc - 1];
}

static inline l_val Cpref(front)(l_con *q)
{
    assert(Cpref(size)(q));
    return q->data[q->tail];
}

static inline l_itr Cpref(begin)(l_con *q)
{
    l_itr it;
    it.a = q;
    it.ref = q->data + q->tail;
    return it;
}

static inline l_itr Cpref(end)(l_con *q)
{
    l_itr it;
    it.a = q;
    it.ref = q->data + q->head;
    return it;
}

static inline l_itr Cpref(next)(l_itr it)
{
    l_itr i2;
    i2.a = it.a;
    i2.ref = it.ref + 1;
    if (i2.ref == it.a->data + it.a->alloc) i2.ref = it.a->data;
    return i2;
}

static inline int Cpref(itr_eq)(l_itr it, l_itr jt)
{
    return it.ref == jt.ref;
}

#undef Ccat3
#undef Ccat2
#undef Cpref
#undef l_con
#undef l_val
#undef l_itr
#undef l_valdrop
#undef l_valclone


