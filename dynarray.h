#if !defined(l_val)
#error "Define l_val before including"
#endif

#if !defined(l_con)
#define l_con Ccat2(dynarr, l_val)
#endif

#define l_itr Ccat2(l_con, itr)

#if (defined(l_valdrop) && !defined(l_valclone)) || (defined(l_valclone) && !defined(l_valdrop))
#error "l_valdrop and l_valclone are mutually inclusive"
#endif


#ifndef l_valdrop
#define l_valdrop
#endif
#ifndef l_valclone
#define l_valclone(x) x
#endif

#define Ccat3(a,b) a##_##b
#define Ccat2(a,b) Ccat3(a,b)
#define Cpref(x) Ccat2(l_con,x)

#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>

typedef struct
{
    size_t length;
    size_t alloc;
    l_val *data;
} l_con;

typedef struct
{
    l_con *a;
    l_val *ref;
} l_itr;


static inline l_con Cpref(init)(void)
{
    l_con v;
    v.length = 0;
    v.alloc = 0;
    v.data = NULL;
    return v;
}

static inline l_itr Cpref(begin)(l_con *v)
{
    l_itr it;
    it.a = v;
    it.ref = v->data;
    return it;
}

static inline l_itr Cpref(end)(l_con *v)
{
    l_itr it;
    it.a = v;
    it.ref = v->data + v->length;
    return it;
}

static inline l_itr Cpref(next)(l_itr i)
{
    l_itr it;
    it.a = i.a;
    it.ref = i.ref + 1;
    return it;
}

static inline l_itr Cpref(prev)(l_itr i)
{
    l_itr it;
    it.a = i.a;
    it.ref = i.ref - 1;
    return it;
}

static inline int Cpref(itr_eq)(l_itr it, l_itr jt)
{
    return it.ref == jt.ref;
}

static inline void Cpref(grow)(l_con *v, size_t n)
{
    if (v->length + n >= v->alloc)
    {
        v->alloc = v->alloc ? v->alloc * 2 : 32;
        if (v->alloc < v->length + n)
            v->alloc = v->length + n;
        v->data = (l_val*)realloc(v->data, sizeof(v->data[0]) * v->alloc);
    }
}

static inline void Cpref(push)(l_con *v, l_val x)
{
    Cpref(grow)(v, 1);
    v->data[v->length++] = l_valclone(x);
}

static inline l_val Cpref(top)(l_con *v)
{
    assert(v->length);
    return v->data[v->length - 1];
}

static inline void Cpref(pop)(l_con *v)
{
    if (v->length == 0) return;
    v->length--;
    l_valdrop(&v->data[v->length]);
}

static inline void Cpref(erase)(l_con *v, size_t i)
{
    assert(v->length > i);
    memmove(v->data + i, v->data + i + 1,
            sizeof(v->data[0]) * (v->length - 1 - i));
    v->length--;
    l_valdrop(&v->data[v->length]);
}

static inline void Cpref(insert)(l_con *v, size_t i, l_val x)
{
    Cpref(grow)(v, 1);
    assert(v->length > i);
    memmove(v->data + i + 1, v->data + i,
            sizeof(v->data[0]) * (v->length - i));
    v->data[i] = l_valclone(x);
    v->length++;
}

static inline void Cpref(clear)(l_con *v)
{
    for (size_t i = 0; i < v->length; i++)
        l_valdrop(&v->data[i]);
    v->length = 0;
}

static inline size_t Cpref(size)(l_con *v)
{
    return v->length;
}

static inline l_con Cpref(clone)(l_con v)
{
    l_con w = Cpref(init)();
    for (size_t i = 0; i < v.length; i++)
        Cpref(push)(&w, v.data[i]);
    return w;
}

static inline void Cpref(drop)(l_con *v)
{
    for (size_t i = 0; i < v->length; i++)
        l_valdrop(&v->data[i]);
    free(v->data);
}

#undef Ccat3
#undef Ccat2
#undef Cpref
#undef l_con
#undef l_val
#undef l_valdrop
#undef l_valclone
#undef l_itr

