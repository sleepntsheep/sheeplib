#ifndef SHEEP_BITSET_H
#define SHEEP_BITSET_H

#include <limits.h> 
#include <stddef.h> 

typedef unsigned int bitint;

#define BITINT_BIT (sizeof(bitint) * CHAR_BIT)
#define BITNCNT(n) (((n) + BITINT_BIT - 1) / BITINT_BIT)
void bitset(bitint *a, size_t b);
void bitunset(bitint *a, size_t b);
int bitget(bitint *a, size_t b);

#endif


