#include <ena/algo.h>
#include <ena/bitset.h>
#include <ena/dynarray2.h>
#include "utest.h"

int int_cmp(const void *pa, const void *pb)
{
    int a = *(const int*)pa, b = *(const int*)pb;
    if (a < b) return -1;
    if (a == b) return 0;
    return 1;
}

int char_cmp(const void *pa, const void *pb)
{
    char a = *(const char*)pa, b = *(const char*)pb;
    if (a < b) return -1;
    if (a == b) return 0;
    return 1;
}

UTEST(algo, swap) {
    int a = 2, b = 5;
    l_swap(&a, &b, sizeof a);
    ASSERT_EQ(a, 5);
    ASSERT_EQ(b, 2);
}

UTEST(algo, reverse) {
    char str[] = "ihccoB";
    l_reverse(str, 6, 1);
    ASSERT_STREQ("Bocchi", str);
    char str2[] = "EVEN";
    l_reverse(str2, 4, 1);
    ASSERT_STREQ("NEVE", str2);
    char str3[] = "O0D";
    l_reverse(str3, 3, 1);
    ASSERT_STREQ("D0O", str3);
}

UTEST(algo, next_permutation) {
    char v[] = "123";
    ASSERT_TRUE(l_next_permutation(v, 3, 1, char_cmp));
    ASSERT_STREQ("132", v);
    ASSERT_TRUE(l_next_permutation(v, 3, 1, char_cmp));
    ASSERT_STREQ("213", v);
    ASSERT_TRUE(l_next_permutation(v, 3, 1, char_cmp));
    ASSERT_STREQ("231", v);
    ASSERT_TRUE(l_next_permutation(v, 3, 1, char_cmp));
    ASSERT_STREQ("312", v);
    ASSERT_TRUE(l_next_permutation(v, 3, 1, char_cmp));
    ASSERT_STREQ("321", v);
    ASSERT_FALSE(l_next_permutation(v, 3, 1, char_cmp));
}

UTEST(algo, unique) {
    char v[] = "1122333";
    ASSERT_EQ(3, l_unique(v, 7, 1, char_cmp));
    v[3] = 0;
    ASSERT_STREQ("123", v);

    int i[] = {-1, -2, -3};
    ASSERT_EQ(3, l_unique(i, 3, sizeof(int), int_cmp));
    i[1] = -1;
    ASSERT_EQ(2, l_unique(i, 3, sizeof(int), int_cmp));
    ASSERT_EQ(i[1], -3);
}

#define l_con ID
#define l_val int
#include "../deque.h"

UTEST(deque, 1) {
    ID q = ID_init();
    ASSERT_EQ(ID_size(&q), 0);
    ID_push_back(&q, 1);
    ASSERT_EQ(ID_size(&q), 1);
    ASSERT_EQ(ID_back(&q), 1);
}

UTEST(deque, 2) {
    ID q = ID_init();
    for (int i = 0; i < 10000; i++)
    {
        ID_push_back(&q, i);
        ASSERT_EQ(ID_size(&q), i+1);
        ASSERT_EQ(ID_back(&q), i);
    }
    for (int i = 0; i < 10000; i++)
    {
        ASSERT_EQ(ID_front(&q), i);
        ID_pop_front(&q);
        if (ID_size(&q)) ASSERT_EQ(ID_front(&q), i+1);
    }
    ASSERT_EQ(ID_size(&q), 0);
}

UTEST(deque, 3) {
    ID q = ID_init();
    for (int i = 0; i < 10000; i++)
    {
        ID_push_back(&q, i);
        ASSERT_EQ(ID_size(&q), 1);
        ASSERT_EQ(ID_back(&q), ID_front(&q));
        ID_pop_back(&q);
    }
    ASSERT_EQ(ID_size(&q), 0);
}

UTEST(deque, 4) {
    ID q = ID_init();
    ID_push_front(&q, 0);
    ID_push_back(&q, 1);
    ASSERT_EQ(ID_front(&q), 0);
    ASSERT_EQ(ID_back(&q), 1);
    ASSERT_EQ(ID_size(&q), 2);
    ID_pop_front(&q);
    ASSERT_EQ(ID_front(&q), 1);
    ASSERT_EQ(ID_size(&q), 1);
    
    ID_push_front(&q, -1);
    ID_push_front(&q, -2);
    ID_push_front(&q, -3);
    ASSERT_EQ(ID_front(&q), -3);
    ID_pop_back(&q);
    ASSERT_EQ(ID_back(&q), -1);
    ID_pop_back(&q);
    ASSERT_EQ(ID_back(&q), -2);
    ID_pop_front(&q);
    ASSERT_EQ(ID_front(&q), -2);
}

UTEST(deque, 5) {
    ID q = ID_init();
    for (int i = 0; i < 10000; i++)
    {
        if (ID_size(&q)) ID_pop_front(&q);
        ID_push_front(&q, i);
        ID_push_back(&q, -i);
        ASSERT_EQ(ID_size(&q), i+2);
    }
}

UTEST(deque, 6) {
    const int k = 10;
    ID q = ID_init();
    for (int i = 0; i < 10000; i++)
    {
        ID_push_front(&q, i);
        ID_push_back(&q, -i);
        for (int j = 0; j < k; j++) ID_push_back(&q, -i);
        for (int j = 0; j < k; j++) ID_pop_back(&q);
        ASSERT_EQ(ID_size(&q), 2*i+2);
    }
    ID_clear(&q);
    ASSERT_EQ(ID_size(&q), 0);
}

UTEST(deque, itr) {
    const int k = 10;
    ID q = ID_init();
    for (int i = 0; i < k; i++)
        ID_push_back(&q, i);
    int j = 0;

    for (ID_itr it = ID_begin(&q); !ID_itr_eq(it, ID_end(&q)); it = ID_next(it))
    {
        ASSERT_EQ(j, *it.ref);
        j++;
    }
    ASSERT_EQ(10, j);
    ID_drop(&q);
}

static int parent[20000];
static int pow3[10];

extern int get3(int, int);
inline int get3(int n, int x)
{
    for (; x--; ) n /= 3;
    return n % 3;
}

extern int set3(int, int, int);
inline int set3(int b, int x, int n)
{
	if (get3(b, x)) return b;
	return b + pow3[x] * n;
}

int popcount3(int n, int j)
{
    int c = 0;
    for (; n; n /= 3) c += ((n % 3) == j);
    return c;
}

int win(int B, int id)
{
#define chk(a, b, c) if (get3(B, a) == id && get3(B, b) == id && get3(B, c) == id) return 1;
	chk(0, 3, 6);
	chk(1, 4, 7);
	chk(2, 5, 8);
	chk(0, 1, 2);
	chk(3, 4, 5);
	chk(6, 7, 8);
	chk(2, 4, 6);
	chk(0, 4, 8);
	return 0;
#undef chk
}

char winner(int b)
{
	if (win(b, 1)) return 'X';
	if (win(b, 2)) return 'O';
	return 0;
}

UTEST(deque, special_xo) {
    static int node_id = 0;
    pow3[0] = 1; for (int i = 1; i < 10; i++) pow3[i] = pow3[i-1] * 3;

    ID q = ID_init();
    ID_push_back(&q, 0);
    while (ID_size(&q))
    {
        int b = ID_front(&q);
        node_id++;
        ID_pop_front(&q);
        int x = popcount3(b, 1);
        int o = popcount3(b, 2);

        if (winner(b)) ;
        else if (x + o == 9) ;
        else
        {
            int j = x <= o ? 1 : 2;
            for (int i = 0; i < 9; i++)
            {
                if (!get3(b, i)) 
                {
                    int n = set3(b, i, j);
                    parent[n] = b;
                    ID_push_back(&q, n);
                }
            }
        }
    }

    ASSERT_EQ(node_id, 549946);
}

#define l_con i_pq
#define l_val int
#include "../pqueue.h"

typedef struct ii { int a, b; } ii;
#define l_con ii_pq
#define l_val ii
#include "../pqueue.h"

#include "utest.h"

int ii_cmp(const void *pa, const void *pb)
{
    const ii *a = pa, *b = pb;
    if (a->a == b->a) return a->b < b->b;
    return a->a < b->b;
}

UTEST(pqueue, 1) {
    i_pq q = i_pq_init(int_cmp);
    ASSERT_EQ(i_pq_size(&q), 0);
    i_pq_push(&q, 2);
    ASSERT_EQ(i_pq_top(&q), 2);
    ASSERT_EQ(i_pq_size(&q), 1);
    i_pq_push(&q, 8);
    ASSERT_EQ(i_pq_top(&q), 8);
    ASSERT_EQ(i_pq_size(&q), 2);
    i_pq_push(&q, -1);
    ASSERT_EQ(i_pq_top(&q), 8);
    ASSERT_EQ(i_pq_size(&q), 3);
}

UTEST(pqueue, 2) {
    i_pq q = i_pq_init(int_cmp);
    for (int i = 0; i < 10000; i++)
    {
        i_pq_push(&q, i);
        ASSERT_EQ(i_pq_top(&q), i);
        ASSERT_EQ(i_pq_size(&q), i+1);
    }
}

UTEST(pqueue, 3) {
    i_pq q = i_pq_init(int_cmp);
    for (int i = 10000 , j = 1; i > 0; i--, j++)
    {
        i_pq_push(&q, i);
        ASSERT_EQ(i_pq_top(&q), 10000);
        ASSERT_EQ(i_pq_size(&q), j);
    }
}

UTEST(pqueue, dijkstra_small) {
    ii_pq q = ii_pq_init(ii_cmp);

    int order = 5;
    int adj_matrix[][5] = 
    {
        {99, 1, 1, 1, 99},
        {1, 99, 99, 5, 99},
        {1, 99, 99, 99, 99},
        {1, 5, 99, 99, 2},
        {99, 99, 99, 2, 99}
    };

    int source = 4;
    int dist[5] = {1e9, 1e9, 1e9, 1e9, 1e9};

    dist[source] = 0;
    ii_pq_push(&q, (ii){0, source});
    while (ii_pq_size(&q))
    {
        ii cu = ii_pq_top(&q); ii_pq_pop(&q);
        int c = -cu.a; int u = cu.b;
        if (dist[u] != c) continue;

        for (int v = 0; v < order; v++)
        {
            if (c + adj_matrix[u][v] < dist[v])
            {
                dist[v] = adj_matrix[u][v] + c;
                ii_pq_push(&q, (ii){-dist[v], v});
            }
        }
    }

    ASSERT_EQ(3, dist[0]);
    ASSERT_EQ(4, dist[1]);
    ASSERT_EQ(4, dist[2]);
    ASSERT_EQ(2, dist[3]);
    ASSERT_EQ(0, dist[4]);

    ii_pq_drop(&q);
}

#define l_con Iarr
#define l_val int
#include "../dynarray.h"

#define N 1000000

UTEST(dynarray, init) {
    Iarr v = Iarr_init();
    Iarr_drop(&v);
}

UTEST(dynarray, push) {
    Iarr v = Iarr_init();

    for (int i = 0; i < N; i++)
        Iarr_push(&v, i);

    for (int i = 0; i < N; i++)
        ASSERT_EQ(v.data[i], i);

    Iarr_drop(&v);
}

UTEST(dynarray, size) {
    Iarr v = Iarr_init();
    Iarr_push(&v, 0);
    ASSERT_EQ(Iarr_size(&v), 1);
    Iarr_drop(&v);
}

UTEST(dynarray, top) {
    Iarr v = Iarr_init();
    Iarr_push(&v, -1);
    ASSERT_EQ(Iarr_top(&v), -1);
    Iarr_drop(&v);
}

UTEST(dynarray, pop) {
    Iarr v = Iarr_init();
    Iarr_push(&v, -1);
    Iarr_pop(&v);
    ASSERT_EQ(Iarr_size(&v), 0);
    Iarr_drop(&v);
}

UTEST(dynarray, erase) {
    Iarr v = Iarr_init();
    Iarr_push(&v, -1);
    Iarr_push(&v, -2);
    Iarr_push(&v, -3);
    Iarr_erase(&v, 1);
    ASSERT_EQ(Iarr_size(&v), 2);
    ASSERT_EQ(v.data[0], -1);
    ASSERT_EQ(v.data[1], -3);
    Iarr_drop(&v);
}

UTEST(dynarray, insert) {
    Iarr v = Iarr_init();
    Iarr_push(&v, -1);
    Iarr_push(&v, -2);
    Iarr_push(&v, -3);
    Iarr_insert(&v, 1, -4);
    ASSERT_EQ(Iarr_size(&v), 4);
    ASSERT_EQ(v.data[0], -1);
    ASSERT_EQ(v.data[1], -4);
    ASSERT_EQ(v.data[2], -2);
    ASSERT_EQ(v.data[3], -3);
    Iarr_drop(&v);
}

UTEST(dynarray, itr) {
    Iarr v = Iarr_init();
    Iarr v2 = Iarr_init();
    for (int i = 0; i < 10; i++) Iarr_push(&v, i);
    for (Iarr_itr it = Iarr_begin(&v); !Iarr_itr_eq(it, Iarr_end(&v)); it = Iarr_next(it))
        Iarr_push(&v2, *it.ref);
    for (int i = 0; i < 10; i++)
        ASSERT_EQ(i, v2.data[i]);

    Iarr_drop(&v);
}


#include <ena/lstr.h>
#define l_val lstr
#define l_con lstrarr
#define l_valclone lstr_clone
#define l_valdrop lstr_drop
#include <ena/dynarray.h>

UTEST(dynarray, clonedrop) {
    lstrarr a = lstrarr_init();

    // Clone need to happen - the string literal would be allocated and therefore writable
    lstrarr_push(&a, "Test");
    a.data[0][0] = 'B';
    ASSERT_STREQ("Best", a.data[0]);

    // Clone need to happen - accessing after original lstr is dropped must not cause segfault
    lstr s1 = lstr_make("Bocchi");
    lstrarr_push(&a, s1);
    lstr_drop(&s1);
    ASSERT_STREQ("Bocchi", a.data[1]);

    lstrarr_drop(&a);
}

UTEST(bitset, 1) {
    bitint bs[BITNCNT(100)] = { 0 };
    for (int i = 0; i < 99; i++) ASSERT_EQ(0, bitget(bs, i));
    for (int i = 0; i < 99; i++) 
    {
        bitset(bs, i);
        ASSERT_TRUE(bitget(bs, i));
        bitunset(bs, i);
    }
}


UTEST_MAIN();

