#include "log.h"

#ifdef __cplusplus
extern "C" {
#include <cstdarg>
#else
#include <stdarg.h>
#endif

void
sheep__stderr_log(const char *type, const char *file
        , const int line, const char *fmt
        , ...)
{
    fprintf(stderr, "%s: %s:%d: ", type, file, line);
    va_list a;
    va_start(a, fmt);
    vfprintf(stderr, fmt, a);
    va_end(a);
    fprintf(stderr, "\n");
    fflush(stderr);
}

#ifdef __cplusplus
}
#endif

