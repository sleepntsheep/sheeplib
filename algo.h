#ifndef SHEEP_ALGO_H
#define SHEEP_ALGO_H

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

void l_swap(void *a, void *b, size_t size);
void l_reverse(void *begin, size_t nmemb, size_t size);
size_t l_unique(void *begin, size_t nmemb, size_t size, int (*compare)(const void *, const void*));
int l_next_permutation(void *begin, size_t nmemb, size_t size, int (*compare)(const void *, const void*));
void l_shuffle(void *begin, size_t nmemb, size_t size);

#endif

