#if !defined(vec_size)
#error "Define vec_size before including"
#endif
#include <math.h>

#define con_t Ccat4(vec, vec_size)

/* preprocessor magic */
#define Ccat5(a, b) a##b
#define Ccat4(a, b) Ccat5(a, b)
#define Ccat3(a,b) a##_##b
#define Ccat2(a,b) Ccat3(a,b)
#define Cpref(x) Ccat2(con_t,x)

typedef float con_t[vec_size];

void Cpref(add)(con_t c, con_t const a, con_t const b)
{
    for (int i = 0; i < vec_size; i++) c[i] = a[i] + b[i];
}

void Cpref(adds)(con_t a, con_t const b)
{
    for (int i = 0; i < vec_size; i++) a[i] += b[i];
}

void Cpref(sub)(con_t c, con_t const a, con_t const b)
{
    for (int i = 0; i < vec_size; i++) c[i] = a[i] - b[i];
}

void Cpref(subs)(con_t a, con_t const b)
{
    for (int i = 0; i < vec_size; i++) a[i] -= b[i];
}

void Cpref(scale)(con_t c, con_t const a, float s)
{
    for (int i = 0; i < vec_size; i++) c[i] = a[i] * s;
}

void Cpref(scales)(con_t a, float s)
{
    for (int i = 0; i < vec_size; i++) a[i] *= s;
}

void Cpref(dup)(con_t c, con_t const a)
{
    for (int i = 0; i < vec_size; i++) c[i] = a[i];
}

float Cpref(dot)(con_t const a, con_t const b)
{
    float r = 0;
    for (int i = 0; i < vec_size; i++) r += a[i] * b[i];
    return r;
}

float Cpref(abs)(con_t const a)
{
    float r = 0;
    for (int i = 0; i < vec_size; i++) r += a[i] * a[i];
    return sqrt(r);
}

float Cpref(abs_squared)(con_t const a)
{
    float r = 0;
    for (int i = 0; i < vec_size; i++) r += a[i] * a[i];
    return r;
}

void Cpref(unit)(con_t c, con_t const a)
{
    float l = Cpref(abs)(a);
    for (int i = 0; i < vec_size; i++) c[i] = a[i] / l;
}

#undef Ccat5
#undef Ccat4
#undef Ccat3
#undef Ccat2
#undef Cpref
#undef vec_size


