.PHONY : clean

CFLAGS  = -fPIC -pedantic -Wall -Wextra
LDFLAGS = -shared
SOURCES = $(shell echo *.c)
HEADERS = $(shell echo *.h)
TARGET  = libena.so
OBJECTS = $(SOURCES:.c=.o)
DESTDIR ?= /usr/local/lib
TESTSRCS = $(wildcard test/*.c)
TESTS = $(patsubst %.c,%,$(TESTSRCS))

all: $(TARGET)

clean:
	rm -f $(OBJECTS) $(TARGET)

$(TARGET) : $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@ $(LDFLAGS)

install: $(TARGET)
	mkdir -p /usr/local/include/ena
	cp *.h /usr/local/include/ena
	cp -r math /usr/local/include/ena
	mv $(TARGET) $(DESTDIR)

