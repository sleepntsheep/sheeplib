#if !defined(l_con) || !defined(l_val)
#error "Define l_con and l_val before including"
#endif

/* preprocessor magic */
#define Ccat3(a,b) a##_##b
#define Ccat2(a,b) Ccat3(a,b)
#define Cpref(x) Ccat2(l_con,x)

#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

typedef struct
{
    size_t alloc;
    size_t length;
    l_val *data;
    int (*compare)(const void*,const void*);
} l_con;

l_con Cpref(init)(int (*compare)(const void*,const void*))
{
    l_con q;
    q.alloc = 0;
    q.length = 0;
    q.data = NULL;
    q.compare = compare;
    return q;
}

void Cpref(drop)(l_con *q)
{
    free(q->data);
}

static void Cpref(grow)(l_con *q, size_t n)
{
    if (q->length + n >= q->alloc)
    {
        q->alloc = q->alloc ? q->alloc * 2 : 32;
        if (q->alloc < q->length + n)
            q->alloc = q->length + n;
        q->data = (l_val*)realloc(q->data, sizeof(q->data[0]) * q->alloc);
    }
}

static void Cpref(heapify_up)(l_con *q, size_t i)
{
    for (; i && q->compare(&q->data[(i-1)>>1], &q->data[i]) < 0; i = (i-1)>>1)
    {
        l_val temp = q->data[i];
        q->data[i] = q->data[(i-1)>>1];
        q->data[(i-1)>>1] = temp;
    }
}

static void Cpref(decrease_key)(l_con *q, size_t i, l_val x)
{
    q->data[i] = x;
    Cpref(heapify_up)(q, i);
}

void Cpref(push)(l_con *q, l_val x)
{
    Cpref(grow)(q, 1);
    q->data[q->length] = x;
    Cpref(heapify_up)(q, q->length);
    q->length++;
}

l_val Cpref(top)(l_con *q)
{
    assert(q->length);
    return q->data[0];
}

void Cpref(pop)(l_con *q)
{
    assert(q->length);
    if (q->length == 1)
    {
        --q->length;
        return;
    }

    q->data[0] = q->data[--q->length];

    size_t c = 0;
    while (c <= q->length)
    {
        size_t new_root = c;
        for (int j = 1; j <= 2; j++)
            if (2*c+j < q->length && q->compare(&q->data[2*c+j], &q->data[new_root]) < 0)
                new_root = 2*c+j;
        if (c == new_root) break;
        l_val temp = q->data[c];
        q->data[c] = q->data[new_root];
        q->data[new_root] = temp;
        c = new_root;
    }
}

size_t Cpref(size)(l_con *q)
{
    return q->length;
}

size_t Cpref(empty)(l_con *q)
{
    return Cpref(size)(q) == 0;
}

void Cpref(clear)(l_con *q)
{
    q->length = 0;
}

#undef Ccat3
#undef Ccat2
#undef Cpref
#undef l_con
#undef l_val


