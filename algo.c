#include "algo.h"

void l_swap(void *a, void *b, size_t size)
{
    unsigned char temp;
    unsigned char *pa = (unsigned char*)a;
    unsigned char *pb = (unsigned char*)b;
    for (size_t i = 0; i < size; i++) {
        temp = pa[i];
        pa[i] = pb[i];
        pb[i] = temp;
    }
}

void l_reverse(void *begin, size_t nmemb, size_t size)
{
    unsigned char *p = (unsigned char*)begin;
    for (size_t i = 0; i <= (nmemb - 1) / 2; i++)
        l_swap(p + size * i, p + size * (nmemb - 1 - i), size);
}

int l_next_permutation(void *begin, size_t nmemb, size_t size, int (*compare)(const void *, const void*))
{
    unsigned char *p = (unsigned char*)begin;
    if (nmemb < 2) return 0;
    size_t k = 0, l = 0;
    int fk = 0;
    for (k = nmemb - 1; k >= 1; k--)
        if (compare(p + (k - 1) * size, p + k * size) < 0) { fk = 1; k--; break; }

    if (!fk) return 0;

    for (l = nmemb - 1; l > k; l--)
        if (compare(p + size * l, p + size * k) > 0) break;

    if (l <= k) return 0;

    l_swap(p + size * l, p + size * k, size);
    l_reverse(p + size * (1 + k), nmemb - k - 1, size);
    return 1;
}

size_t l_unique(void *begin, size_t nmemb, size_t size, int (*compare)(const void *, const void*))
{
    unsigned char *p = begin;
    size_t l = 1;
    for (size_t i = 1; i < nmemb; i++)
    {
        if (compare(p + size * (i - 1), p + size * i) != 0)
        {
            memcpy(p + size * l, p + size * i, size);
            l++;
        }
    }
    return l;
}

void l_shuffle(void *begin, size_t nmemb, size_t size)
{
    unsigned char *p = (unsigned char*)begin;
    for (size_t i = nmemb; i > 1; i--)
    {
        size_t j = rand() % i;
        l_swap(p + (i - 1) * size, p + j * size, size);
    }
}

